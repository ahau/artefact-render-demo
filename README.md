# artefact render demo

## Setup

You need to be running an `artefact-server`, simplest way to do that is clone that repo down and

```bash
$ npm i
$ npm run dev

Artefact-store starting...
  artefact-store Own hyperdrive ready, driveAddress: 4396 discovery: c917 +0ms
  artefact-store Using noise keypair  a3e0 98b6 +1ms
  artefact-store Hyperswarm is instantiated +13s
store.storagePath: /tmp/artefact-store-1614557516783
store.driveAddress: 4396d256c2996abdde52b0599cefb1557c7d6ba151ddca9b6cdfa22385c14680
Artefact Server listening on http://localhost:1234
```

This starts a dev server using /tmp storage

Then in this repo

```bash
$ npm i
$ npm run dev
```

This creates `index.html` and serves it using a static file server, so you open it in a browser.
Note the files iterate there might not be accessible to you. You might need to put your own details in there

Using [httpie](https://httpie.io/) (or similar) you can sling some http requests at the server:

```bash
$ http POST localhost:1234/blob localFilePath=/home/mixx/Pictures/backgrounds/tony-reid-S1Kc05nMnug-unsplash.jpg

HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 513
Content-Type: application/json; charset=utf-8
Date: Mon, 01 Mar 2021 00:20:17 GMT
Vary: Origin

{
    "blobId": "tony-reid-S1Kc05nMnug-unsplash.jpg",
    "driveAddress": "5abb7c2797d01ce97bc4df724f30cba655c66e9af3246830ed24b73f9e111947",
    "fileName": "tony-reid-S1Kc05nMnug-unsplash.jpg",
    "href": "http://localhost:1234/drive/5abb7c2797d01ce97bc4df724f30cba655c66e9af3246830ed24b73f9e111947/blob/tony-reid-S1Kc05nMnug-unsplash.jpg?readKey=78a0b09af4dd5bc1baf109768214515943539309df2875e3effc1e81db3af181&fileName=tony-reid-S1Kc05nMnug-unsplash.jpg",
    "readKey": "78a0b09af4dd5bc1baf109768214515943539309df2875e3effc1e81db3af181"
}
```

If you have a pataka running at 187.206.144.44:1234, you can register this peers driveAddress (4396d256c2996abdde52b0599cefb1557c7d6ba151ddca9b6cdfa22385c14680) with it for full replication:

```bash
$ http POST 187.206.144.44:1234/drive driveAddress=4396d256c2996abdde52b0599cefb1557c7d6ba151ddca9b6cdfa22385c14680

HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 11
Content-Type: application/json; charset=utf-8
Date: Mon, 01 Mar 2021 00:12:54 GMT
Keep-Alive: timeout=5
Vary: Origin

{
    "ok": true
}
```
