const yo = require('yo-yo')
const render = require('render-media')
const http = require('stream-http')
const { Transform } = require('readable-stream')

const resource = 'localhost'
const port = 1234

function buildOptions (file) {
  const url = new URL(`http://${resource}:${port}/drive/${file.driveAddress}/blob/${file.blobId}`)
  url.search = new URLSearchParams({
    readKey: file.readKey,
    start: file.start,
    end: file.end,
    fileName: file.fileName
  })
  console.log(url.pathname + url.search)
  return {
    method: 'GET',
    hostname: url.hostname,
    port: url.port,
    path: url.pathname + url.search
  }
}

function createStreamFile (file) {
  return {
    name: file.fileName,
    createReadStream: function (opts = {}) {
      file.start = opts.start || 0
      file.end = opts.end
      const transform = new Transform({
        transform (chunk, enc, callback) {
          this.push(chunk)
          callback()
        }
      })
      const req = http.request(buildOptions(file), (res) => {
        console.log(`STATUS: ${res.statusCode}`)
        res.pipe(transform)
      })
      req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`)
      })
      req.end()
      return transform
    }
  }
}

const files = [
  {
    driveAddress: 'e1c3f8e4c191babb3f926182e066b9d9994bb7d5f10ed16412251ffa7ff12798',
    blobId: 'ira-mix-cradle.jpeg',
    readKey: 'da797f4b4e0da233e2d60c672c685308126baba757d454d9711cd79104ce3ec6',
    fileName: 'ira-mix-cradle.jpeg'
  },
  {
    driveAddress: 'e1c3f8e4c191babb3f926182e066b9d9994bb7d5f10ed16412251ffa7ff12798',
    blobId: 'Sintel.mp4',
    readKey: '7a5edca403fee856b803b4f05c950337ebeba98870e2aa35d63f0803cf85a6f0',
    fileName: 'Sintel.mp4'
  },
  {
    driveAddress: 'acb9d995b172a035f61ec8c42419b72c31742d9a1c586a46a703f33433a36806',
    blobId: 'NAEST 108 01 - Mavor 50 year history booklet 1931.pdf',
    readKey: 'e630c26729207ce8489564341e0dd557db977f61558da1a1231ee500f463ed9d',
    fileName: 'NAEST 108 01 - Mavor 50 year history booklet 1931.pdf'
  },
  {
    driveAddress: 'f2c4ca68b2b29a0873b111fd1e51f09ffee6a264fa6eb25aecfadb8a6e17456c',
    blobId: 'tony-reid-S1Kc05nMnug-unsplash.jpg',
    readKey: 'cfdcb7556ade2a3f4ab863606fc8b0472659649512a718aecfeb828e5e1f3203',
    fileName: 'tony-reid-S1Kc05nMnug-unsplash.jpg'
  },
  {
    driveAddress: 'f2c4ca68b2b29a0873b111fd1e51f09ffee6a264fa6eb25aecfadb8a6e17456c',
    blobId: 'Frozen.2013.1080p.BluRay.x264.YIFY.mp4',
    readKey: 'ee0abe00d19d7fc6648dee495b6a5cf72eb6548d502d8882417ba0859d5d120f',
    fileName: 'Frozen.2013.1080p.BluRay.x264.YIFY.mp4'
  }
]

const App = () => yo`<div>
  <div style='margin-bottom: 50px;'>
    ${files.map(function (item) {
      return yo`<button onclick=${onClick(item)}>${item.fileName}</button>`
    })}
  </div>

  <div id='appendTarget' style='display: flex; flex-wrap: wrap;'>
  </div>
</div>`

function onClick (file) {
  return function () {
    render.append(createStreamFile(file), '#appendTarget', function (err, elem) {
      if (err) return console.error(err.message)

      // elem.height = 600
      elem.style.minWidth = '400px'
      elem.style.minHeight = '400px'
      elem.style.maxHeight = '600px'
      elem.style.margin = '10px'
    })
  }
}

document.body.appendChild(App())
